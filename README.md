# Python Panamá Junio 2024

## Usando Servicios Administrados de AI de AWS con Python y Boto3

![Python Panamá](images/pythonpanama.png)

El Jupyter Notebook de la charla es [panama20240618.ipynb](panama20240618.ipynb).

[panama20240618_final.ipynb](panama20240618_final.ipynb) es el Notebook ya "ejecutado", en el caso de que solo quieras ver el resultado final.

## Requirementos para ejecutar

- Python 3.12.\*
- Poetry
- Una cuenta de AWS con permisos para:
  - Comprehend
  - Polly
  - Rekognition
  - Transcribe
  - Translate

Nota: La capa gratuita de AWS soporta todas las llamadas sin que se tenga que hacer algún pago.

## Instrucciones

Ejecutar en la consola:

```bash
poetry install
poetry shell
jupyter notebook panama20240618.ipynb
```

## Abrir en [Binder](https://mybinder.org)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/soldavid%2Fpythonpanama20240618/main?labpath=panama20240618.ipynb)

## Muchas gracias
